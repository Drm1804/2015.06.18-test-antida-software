var documentApp = angular.module('documentApp', []);

documentApp.controller('editorCtrl', function($scope){

    //Проверяем есть ли данные в localStorage
    var dataLocStorage = localStorage.data;
    if(!(dataLocStorage)){
        var data = angular.toJson(dataDocument);
        localStorage.setItem('data', data);
        $scope.datas =  dataDocument;
        dataLocStorage = localStorage.data;
    } else {

        $scope.datas =  angular.fromJson(dataLocStorage);
        dataLocStorage = $scope.datas
    }

    $scope.checkDoc = function (data){
        $('textarea').hide();
        $('.b-edit-area--'+data.id+'').show();
        $('.b-item-doc-link').removeClass('b-item-doc-link--checked');
        $('.b-item-doc-link--'+data.id+'').addClass('b-item-doc-link--checked');
    };

    //Событие по клику "Отчистить"
    $scope.clearArea = function(){
        $('textarea').hide();
        $('.b-item-doc-link').removeClass('b-item-doc-link--checked');
        $('textarea:first').show().val('');
        //Возвращаем список в начало
        $('.b-editor__list-doc').animate({ scrollTop: 0 }, "slow");
    };

    //Событие по клику "Добавить документ"
    $scope.addDocument = function(){
        var text = $('textarea:visible').val();
        $('.b-pop-up').show();

        //Ожидаем, пока пользователь не закроет окно или не нажмет кнопку "Отправить"
        $scope.closePopUp = function(){
            $('.b-pop-up').hide();
            var pop = $('.b-pop-up-input');
            var name = pop.val();
            pop.val('');
            $scope.returnAddDocument(text, name);
        };

        //Продолжаем добавлять новый документ
        $scope.returnAddDocument = function(text, name){
            var length = $scope.datas.length;
            var id = 0;

            //Определяем как будет выглядеть id элемента в базе
            if(length < 9){
                id = '000'+(length+1);
            } else if(length < 99){
                id = '00'+(length+1);
            } else if(length < 999){
                id = '0'+(length+1);
            } else {
                id = length;
            }

            //Если имя не задано, придумываем имя за пользователя
            if(name == ''){
                name = 'Документ '+ (length + 1);
            }
            //Пушим новый элемент в массив с данными angular
            $scope.datas.push({id: ''+id+'', name: ''+name+'', text: ''+text+''});

            writeSorage($scope.datas);
            //Имитируем клик по новому блоку, чтобы он оказался выбранным (Таймаут нужен, чтобы DOM успел обновиться)
            setTimeout(function(){
                $('.b-item-doc-link--'+id+'').click();
                var list = $('.b-editor__list-doc');
                var scrollValue = list.height();
                list.animate({ scrollTop: scrollValue }, "slow");
            }, 100);

            //
            //В этом месте мы запишем данные в Куку, чтобы все сохранилось
            //


        }


    };

    //Автосохранение

    $scope.autoSaveNewArea = function(){
        var text = $('textarea:visible').val();

        if(text != ''){
            $scope.addDocument();
        }

    };

    $scope.changeArea = function(){
        writeSorage(dataLocStorage);
    }
});

function writeSorage(dataLocStorage){
    var newData = dataLocStorage;
    if(typeof(newData) == 'string'){
        newData = Object(newData);
        newData = JSON.parse(newData)
    }
    localStorage.removeItem('data');

    console.log(newData);
    newData = $.toJSON(newData);
    localStorage.setItem('data', newData);
}
